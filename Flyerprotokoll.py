import folium
import pandas as pd
from geopy.geocoders import Nominatim
from datetime import datetime
import re
import math
import requests
import io
import sys


city = 'Graz'
input_file = 'Flyerprotokoll Graz.xlsx'
marker_scale = 3
output_csv = True
csv_file_name = 'data.csv'
use_local_xlsx = False
xlsx_file_name = 'data.xlsx'

# the function that scales the markers, you can add or remove the log
def scale_marker(size):
    return 5
    # return math.log(size * marker_scale)
    # return size * marker_scale

if '--skip-download' in sys.argv:

    # Read the Excel file into an ExcelFile object
    xls = pd.ExcelFile(xlsx_file_name)
    print("Skipped download, successfully read xlsx file from disk")

else:

    sheet_id = '1r_Maxtmx_gZttojl-NmsfmqQ0-Xe9KWWwwplIRXRq_Y'
    url = f'https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=xlsx'

    response = requests.get(url)
    if response.status_code != 200:
        print(f"Failed to download the file, code {response.status_code} {response.reason}")
        exit(-1)
    print("Successfully downloaded from google sheets")

    # Create a Bytes buffer from the response content
    excel_buffer = io.BytesIO(response.content)

    # Read the Excel file directly from the buffer
    xls = pd.ExcelFile(excel_buffer)

    with open(xlsx_file_name, 'wb') as f:
        f.write(excel_buffer.getvalue())
    print(f"File saved successfully at {xlsx_file_name}")


def expand_address_range(address_string):

    first_numeric_index = next((i for i, char in enumerate(address_string) if char.isdigit()), None)
    if first_numeric_index is None:
        return address_string
    address_string = address_string[first_numeric_index:]

    # Split the string by commas and strip spaces
    components = [component.strip() for component in address_string.split(',')]

    expanded_addresses = []

    for component in components:
        # Check for a range indicated by a hyphen
        if '-' in component:
            # Separate the base part and the range part
            base, range_part = component.split('-')

            # Check if the range part has a letter (like 'a-e')
            if range_part.isalpha():
                # Generate addresses for each letter in the range
                for letter in range(ord(base[-1]), ord(range_part[0]) + 1):
                    expanded_addresses.append(base[:-1] + chr(letter))
            else:
                # Generate numeric range
                for number in range(int(base), int(range_part) + 1):
                    expanded_addresses.append(str(number))
        else:
            # If not a range, add the component as it is
            expanded_addresses.append(component)

    return expanded_addresses

def get_addresses(input_str):

    if not isinstance(input_str, str):
        print('Error: tried to get address of :' + str(input_str))
        return ""

    # Use a regular expression to check if the string contains a number
    if not re.search(r'\d', input_str):
        return [item.strip() for item in input_str.split(',')]

    street = input_str
    # Use a regular expression to match everything from the beginning of the string up to the first number
    match = re.match(r'^[^\d]*', input_str)
    if match:
        street = match.group(0).strip()  # Trim any leading or trailing spaces

    house_numbers = expand_address_range(input_str)

    # Use map to add the street to each house number
    return [f'{street} {house_number}' for house_number in house_numbers]

# Function to get the last non-empty value above in a specific column
def get_last_non_empty_value_above(values, current_row, column_index):
    for i in range(current_row, -1, -1):
        if not pd.isna(values[i][column_index]):
            return values[i][column_index]
    return ''  # Return an empty string if no non-empty value is found



# Create a dictionary to store DataFrames, where keys are sheet names
sheets = {}

csv_data = []

# Read each sheet into a DataFrame and store it in the dictionary
for sheet_name in xls.sheet_names:
    sheets[sheet_name] = pd.read_excel(xls, sheet_name)

print('Aggregate sheet data')


# Iterate over the sheets and perform operations on each DataFrame
for sheet_name, sheet_data in sheets.items():
    # Get values in the data range
    values = sheet_data.values

 #   if sheet_name != 'BeispielScript':
 #       continue
    
    if sheet_name == "Auswertung":
        continue

    # Loop through each row in the values starting from row 3
    for j in range(1, len(values)):
        # Check if the second column is not empty
        if values[j][1] != '' and not pd.isna(values[j][1]):
            addresses = get_addresses(values[j][1])

            # If the second column is empty, take the value from the last non-empty cell above
            last_non_empty_date = get_last_non_empty_value_above(values, j, 0)
            last_non_empty_method = get_last_non_empty_value_above(values, j, 3)

            if last_non_empty_method != 'Wohnungstüren':
                continue

            # Loop through each address and append a separate row for each house number
            for k in range(len(addresses)):
                # Append data to the data list
                csv_data.append([last_non_empty_date, addresses[k] + " Graz", values[j][2]])
                # we want the value only to be written once, we will average it out later
                values[j][2] = ''

        elif values[j][0] != '' and not pd.isna(values[j][0]):
            continue
        else:
            break
    print(sheet_name)


print('Finished aggregating sheet data')

if len(csv_data) == 0:
    print("Aggregated data is empty")

column_index = 2

#calculate the average anzahl
# Loop through each row in reverse order
for i in range(len(csv_data) - 1, -1, -1):
    if pd.isna(csv_data[i][column_index]):
        continue
    if csv_data[i][column_index] == '':
        continue
    # If the cell is non-empty, calculate the average for each subsequent empty cell
    current_cell_value = csv_data[i][column_index]
    empty_cell_count = 1
    sum_value = current_cell_value

    for j in range(i + 1, len(csv_data)):
        if not isinstance(csv_data[j][column_index], (int, float, complex)) or pd.isna(csv_data[j][column_index]):
            empty_cell_count += 1
        else:
            break

    average = round(sum_value / (empty_cell_count))

    for j in range(i, i + empty_cell_count):
        csv_data[j][2] = average

# Convert processed data to a DataFrame
data = pd.DataFrame(csv_data, columns=['Datum', 'Ort', 'Anzahl'])

old_csv_data = pd.read_csv(csv_file_name)

##########################################

geolocator = Nominatim(user_agent="Flyerprotokoll")

city_location = geolocator.geocode(city)
map_center = [city_location.latitude, city_location.longitude]

# Add geocoded latitude and longitude columns to your DataFrame
data['latitude'] = None
data['longitude'] = None

print('Start geolocation')

# Iterate through your data and geocode addresses
for index, row in data.iterrows():

    address = row['Ort']

    filter = old_csv_data['Ort'].isin([address])
    matching_row = old_csv_data[filter]

    if not matching_row.empty:
        lat = matching_row.iloc[0]['latitude']
        long = matching_row.iloc[0]['longitude']

        if not pd.isna(lat) and not pd.isna(long):
            data.at[index, 'latitude'] = lat
            data.at[index, 'longitude'] = long

            print('[{}] {}/{}'.format('-'*int(index/len(data)*60), index, len(data)), end='\r')
            continue

    location = geolocator.geocode(address)

    if location:

        if location.latitude is None or location.longitude is None:
            location = geolocator.geocode(address + ', ' + city)

        data.at[index, 'latitude'] = location.latitude
        data.at[index, 'longitude'] = location.longitude
    print('[{}] {}/{}'.format('='*int(index/len(data)*60), index, len(data)), end='\r')

print('\nFinished geolocation')


# Save the processed data to a CSV file
if output_csv:
    data.to_csv('data.csv', index=False)
    print('exported to data.csv')



# Define a function to calculate the age of the date
def calculate_age(date_string):
    date_object = datetime.strptime(date_string, '%d.%m.%Y')
    current_date = datetime.now()
    age = (current_date - date_object).days
    return age

# Find the newest and oldest dates in the data
newest_age = calculate_age(data['Datum'].max())
oldest_age = calculate_age(data['Datum'].min())

# Function to map age to a gradient color
def age_to_color(age):
    # Normalize age to the range [0, 1]
    normalized_age = (age - newest_age) / (oldest_age - newest_age)

    # Interpolate color based on normalized age
    blended_color = (
        int(normalized_age * 255),          # Red component
        int((1 - normalized_age) * 155),    # Green component
        0                                   # Blue component (set to 0 for simplicity)
    )

    return f'rgba({blended_color[0]}, {blended_color[1]}, {blended_color[2]}, 0.5)'


# Create a map centered at an initial location
my_map = folium.Map(location=map_center, zoom_start=12)

# Create layers for geocoded and not found addresses
geocoded_layer = folium.FeatureGroup(name='Geocoded Addresses')
not_found_layer = folium.FeatureGroup(name='Addresses Not Found')

not_found_counter = 0

# Only keep the newest entry for a specific address
data['Datum'] = pd.to_datetime(data['Datum'], format='%d.%m.%Y') # Make Datum numeric, so that idxmax() works
newest_entries_index = data.groupby('Ort')['Datum'].idxmax()
data['Datum'] = data['Datum'].dt.strftime('%d.%m.%Y') # Convert Datum back to string
data = data.loc[newest_entries_index]

# Iterate through your data and add CircleMarkers to the map
for index, row in data.iterrows():

    if row.latitude is None or row.longitude is None:
        folium.Marker(
                location=[x + not_found_counter * 0.0001 for x in map_center],  # put the marker in the center with a lil offset
                popup=f"Address not found: {row['Ort']}",
                icon=folium.Icon(color='red'),
            ).add_to(not_found_layer)
        print('Failed to find ' + row['Ort'])
        not_found_counter += 1
        continue

    # Extract date, address, and amount from each row
    date = row['Datum']
    address = row['Ort']
    amount = row['Anzahl']

    # Calculate the age of the date
    age = calculate_age(date)

    # Convert age to a color using the gradient function
    color = age_to_color(age)

    # Create a CircleMarker with size based on the amount and color based on the date
    folium.CircleMarker(
        location=[row.latitude, row.longitude],  # Use the geocoded coordinates of the address
        radius= scale_marker(amount),  # Adjust the radius based on the amount
        color=color,
        opacity=0.6,
        fill=True,
        fill_color=color,
        fill_opacity=0.6,
        popup=f"Date: {date}<br>Address: {address}<br>Amount: {amount}",
    ).add_to(geocoded_layer)

folium.CircleMarker(
        location=[47.08007775, 15.451004486884523],  # Use the geocoded coordinates of the address
        radius= 15,  # Adjust the radius based on the amount
        color='#a2b81f',
        fill=True,
        fill_color='#0a5b74',
        fill_opacity=1,
        popup=f"Infoabend Cafe Liebig<br>19.12.2023",
    ).add_to(geocoded_layer)

# Add layers to the map
geocoded_layer.add_to(my_map)
not_found_layer.add_to(my_map)

# Add a layer control to switch between layers
folium.LayerControl(collapsed=False).add_to(my_map)

# Save or display the map
my_map.save('public/index.html')

print('public/index.html')
